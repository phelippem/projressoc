<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Presidio::class, function (Faker\Generator $faker) {

    return [
        'nome' => $faker->name,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Presidiario::class, function (Faker\Generator $faker) {

	$profissao = \App\Profissao::all()->random();
	$presidio = \App\Presidio::all()->random();

    /*return [
		'nome' => $faker->name,
		'naturalidade' => $faker->word,
		'genero' => $faker->word,
		'profissao_id' => $profissao->id,
		'regime' => $faker->word,
		'presidio_id' => $presidio->id,
		'prontuario_key' => $faker->numberBetween(0,10000),
		'perfil_profissional' => $faker->sentences(4),
	];*/
    return [
		'nome' => $faker->name,
		'data_nasc' => $faker->dateTimeBetween($startDate = '-45 years', $endDate = '-18 yeras', $timezone = date_default_timezone_get())->format('d/m/Y'),
		'naturalidade' => $faker->word,
		'genero' => $faker->randomElement($array = array ('1','2')),
		'profissao_id' => \App\Profissao::all()->random()->id,
		'regime' => $faker->numberBetween(1,3),
		'presidio_id' => \App\Presidio::all()->random()->id,
		'prontuario_key' => $faker->numberBetween(0,10000),
		'perfil_profissional' => $faker->sentence(4),
	];
});
