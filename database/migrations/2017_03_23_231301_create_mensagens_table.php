<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensagensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mensagens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('razao_social')->nullable();
            $table->string('nome_fantasia');
            $table->string('cnpj')->nullable();
            $table->string('segmento');
            $table->integer('presidio_id')->nullable();
            $table->integer('presidiario_id')->nullable();
            $table->string('ano_fundacao');
            $table->string('diretor_responsavel');
            $table->string('tel');
            $table->string('email');
            $table->string('endereco');
            $table->string('numero');
            $table->string('bairro');
            $table->string('observacao');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mensagens');
    }
}
