<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvolucaoPresidiariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evolucao_presidiarios', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto');
            $table->integer('conceito');
            $table->integer('presidiario_id')->unsigned();
            $table->foreign('presidiario_id')->references('id')->on('presidiarios');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('evolucao_presidiarios', function(Blueprint $table){
			$table->dropForeign('evolucao_presidiarios_presidiario_id_foreign');
		});
        Schema::dropIfExists('evolucao_presidiarios');

    }
}
