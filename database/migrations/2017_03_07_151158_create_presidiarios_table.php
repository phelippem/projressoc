<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePresidiariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presidiarios', function (Blueprint $table) {

            $table->increments('id');
            $table->string('nome');
            $table->date('data_nasc');
            $table->string('naturalidade');
            $table->string('genero');
            $table->integer('profissao_id');
            $table->integer('regime');
            $table->integer('presidio_id')->nullable();
			$table->string('prontuario_key');
			$table->text('perfil_profissional');
			$table->timestamps();
			$table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('presidiarios');
    }
}
