<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableToMessagesPresidioId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mensagens', function (Blueprint $table) {
            /*$table->integer('presidio_id')->unsigned()->nullable()->change();
            $table->integer('presidiario_id')->nullable()->change();*/
            #DB::statement('UPDATE `mensagens` SET `presidio_id` = 0 WHERE `user_id` IS NULL;');
            DB::statement('ALTER TABLE `mensagens` MODIFY `presidio_id` INTEGER UNSIGNED NULL;');
            DB::statement('ALTER TABLE `mensagens` MODIFY `presidiario_id` INTEGER UNSIGNED NULL;');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mensagens', function (Blueprint $table) {
            /*$table->integer('presidio_id')->unsigned()->nullable(false)->change();
            $table->integer('presidiario_id')->unsigned()->nullable(false)->change();*/
        });
    }
}
