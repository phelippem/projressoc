<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->call(ProfissoesTableSeeder::class);
		$this->call(PresidiosTableSeeder::class);
		$this->call(PresidiariosTableSeeder::class);
		$this->call(UsersTableSeeder::class);
		$this->call(EvolucaoPresidiarioTableSeeder::class);

	}
}
