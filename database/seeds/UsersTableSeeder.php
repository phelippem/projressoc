<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert([
			'name' => 'Admin do sistema',
			'email' => 'admin@email.com',
			'password' => bcrypt('1234'),
			'role' => 1,
			'presidio_id' => 1
		]);
		DB::table('users')->insert([
			'name' => 'Diretor fulano de tal',
			'email' => 'diretor@email.com',
			'password' => bcrypt('1234'),
			'role' => 2,
			'presidio_id' => 2
		]);
    }
}
