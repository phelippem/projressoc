<?php

use Illuminate\Database\Seeder;

class PresidiariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = \Faker\Factory::create('pt_BR');

		/*DB::table('presidiarios')->insert([
			'nome' => $faker->name,
			'naturalidade' => $faker->word,
			'genero' => $faker->word,
			'profissao_id' => \App\Profissao::all()->random()->id,
			'regime' => $faker->word,
			'presidio_id' => \App\Presidio::all()->random()->id,
			'prontuario_key' => $faker->numberBetween(0,10000),
			'perfil_profissional' => $faker->sentence(4),
		]);*/

		factory(\App\Presidiario::class, 100)->create();
		for($i=0 ; $i<=30 ; $i++):
			//$nasc = $faker->dateTimeBetween($startDate = '-45 years', $endDate = '-18 yeras', $timezone = date_default_timezone_get());

			factory(\App\Presidiario::class)->create(
				[
					'nome' => $faker->name,
					'data_nasc' => $faker->dateTimeBetween($startDate = '-45 years', $endDate = '-18 yeras', $timezone = date_default_timezone_get())->format('d/m/Y'),
					'naturalidade' => $faker->word,
					'genero' => $faker->randomElement($array = array ('1','2')),
					'profissao_id' => \App\Profissao::all()->random()->id,
					'regime' => 3,
					//'presidio_id' => \App\Presidio::all()->random()->id,
					'prontuario_key' => $faker->numberBetween(0,10000),
					'perfil_profissional' => $faker->sentence(4),
				]
			);
		endfor;
    }
}
