<?php

use Illuminate\Database\Seeder;

class ProfissoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$profissoes = [
			['nome' => 'Serviços gerais'],
			['nome' => 'Secretaria'],
			['nome' => 'Vendedora'],
			['nome' => 'Cozinheira'],
			['nome' => 'Embaladora'],
			['nome' => 'Costureira'],
			['nome' => 'Auxiliar de cozinha'],
			['nome' => 'Técnica de enfermagem'],
			['nome' => 'Enfermeira'],
			['nome' => 'Empregada domestica'],
			['nome' => 'Camareira'],
			['nome' => 'Cabeleireira'],
			['nome' => 'Manicure'],
			['nome' => 'Caixa de supermercado'],
			['nome' => 'Costureiro de máquina reta e overloque'],
			['nome' => 'Modelista de roupas'],
			['nome' => 'Padeiro'],
			['nome' => 'Mecânico de máquinas de costura'],
			['nome' => 'Eletricista de automóveis'],
			['nome' => 'Eletricista instalador predial de baixa tensão'],
			['nome' => 'Pedreiro de alvenaria'],
			['nome' => 'Operador de computador'],
			['nome' => 'Encanador instalador predial'],
			['nome' => 'Costureiro de máquina reta e overloque'],
		];
		DB::table('profissoes')->insert($profissoes);
    }
}
