<?php

use Illuminate\Database\Seeder;

class EvolucaoPresidiarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = \Faker\Factory::create('pt_BR');
		$presidiarios = \App\Presidiario::all();

		for($i = 0; $i< 300 ; $i++) {
			DB::table('evolucao_presidiarios')->insert([
				'texto' => $faker->sentence(10),
				'conceito' => $faker->numberBetween(1, 4),
				'presidiario_id' => $presidiarios->random()->id,
				'created_at' => $faker->dateTime(),
			]);
		}
    }
}
