<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIfUserRoleAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

		if ( Auth::check() && Auth::user()->role == 1 )
		{
			return $next($request);
		}

		//return ['error'=>'Access forbidden'];
		//abort(403, 'Você não tem permissão para acessar esta área');
		//Log::error('Você não tem permissão para acessar esta área');

		return redirect('home')->withErrors('Você não tem permissão para acessar esta área');

	}
}
