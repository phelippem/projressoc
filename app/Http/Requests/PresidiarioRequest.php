<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PresidiarioRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
	public function rules()
	{

		$rules = [];

		#dd($this->method());

		switch($this->method())
		{
			case 'GET':
			case 'DELETE':
			{
				return [];
			}
			case 'POST':
			{
				return [
					'regime' => 'required',
					'presidio_id' => 'required_if:regime,1|required_if:regime,2',
					'nome' => 'required',
					'data_nasc' => 'required|date_format:d/m/Y',
					'naturalidade' => 'required',
					'genero' => 'required',
					'prontuario_key' => 'required',
					'perfil_profissional' => 'required',
					'profissao_id' => 'required',
				];
			}
			case 'PUT':
			case 'PATCH':
			{
				return [
					'regime' => 'required',
					'presidio_id' => 'required_if:regime,1|required_if:regime,2',
					'nome' => 'required',
					'data_nasc' => 'required|date_format:d/m/Y',
					'naturalidade' => 'required',
					'genero' => 'required',
					'prontuario_key' => 'required',
					'perfil_profissional' => 'required',
					'profissao_id' => 'required',
				];
			}
			default:break;
		}
		#dd($rules, $this->method());

		#return $rules;
	}
}
