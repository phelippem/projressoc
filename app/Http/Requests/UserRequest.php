<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		#$user = User::find($this->users);

		$rules = [];

		#dd($this->method());
		switch($this->method())
		{
			case 'GET':
			case 'DELETE':
			{
				$rules = [];
			}
			case 'POST':
			{
				/*$rules = [
					'user.name.first' => 'required',
					'user.name.last'  => 'required',
					'user.email'      => 'required|email|unique:users,email',
					'user.password'   => 'required|confirmed',
				];*/
				$rules = [
					'user.name.first' => 'required',
					'user.name.last'  => 'required',
					'user.email'      => 'required|email|unique:users,email',
					'user.password'   => 'required|confirmed',
				];
			}
			case 'PUT':
			case 'PATCH':
			{
				//dd($this->route('id'));


				$rules = [
					'name' => 'required|min:3',
					'email'      => 'required|email|unique:users,email,'.$this->route('id'),
					'password'   => 'nullable|min:8|confirmed',
				];
			}
			default:break;
		}

		return $rules;
    }
}
