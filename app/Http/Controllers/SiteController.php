<?php

namespace App\Http\Controllers;

use App\Presidiario;
use App\Profissao;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class SiteController extends Controller
{
	private $presidiario;
	private $profissao;

	public function __construct(Profissao $profissao, Presidiario $presidiario)
	{
		$this->presidiario = $presidiario;
		$this->profissao = $profissao;
	}

	public function index()
    {
    	$profissoes = $this->profissao->all();

        return view('site.index', compact('profissoes'));
    }

	public function resultados()
	{
		#dd(request());

        //$profissoes = $this->profissao->all();

        #$pres_nome = Input::get('reeducando_nome');
        $pres_idade = Input::get('reeducando_idade');
        $pres_sexo = Input::get('reeducando_sexo');
        $pres_profissao = Input::get('reeducando_profissao');
        $pres_regime = Input::get('reeducando_regime');


        #$preso = DB::table('presidiarios')->where('nome', 'LIKE', '%'.$pres_nome.'%');
        #$preso = $this->presidiario->where('nome', 'LIKE', '%'.$pres_nome.'%')->with(['profissao', 'presidio', 'evolucoes']);
        $presos = $this->presidiario/*->where('profissao_id', $pres_profissao)*/->with(['profissao', 'presidio', 'evolucoes']);

        #dd($preso->get());

        if($pres_idade) {
            $now = Carbon::now();

            $age_range = explode('-', $pres_idade);

            $age1 = $now->copy()->subYears($age_range[0]);
            $age2 = $now->copy()->subYears($age_range[1]);

            #dd($age1, $age2, $age_range);

            $presos = $presos->whereBetween('data_nasc', [$age2, $age1]);
            #dd($preso->get());

            /*$preso = $preso->where('data_nasc', '<=',$age2);
            $preso = $preso->where('data_nasc', '>=',$age1);*/
        }

        if($pres_sexo){
            $presos = $presos->where('genero', $pres_sexo);
        }

        if($pres_profissao){
            $presos = $presos->where('profissao_id', $pres_profissao);
        }

        if($pres_regime){
            $presos = $presos->where('regime', $pres_regime);
        }

        $presos = $presos->where('status', true);


		$results = $presos->paginate(2);

        #dd($results);


        return view('site.listagem', compact('results', 'pres_idade', 'pres_sexo', 'pres_profissao'))
            ->with([
                'i' => (request()->input('page', 1) - 1) * 5,
                'reeducando_idade' => $pres_idade,
                'reeducando_sexo' => $pres_sexo,
                'reeducando_profissao' => $pres_profissao
            ]);

    }

    public function contactar()
    {
        return view('site.contactar');
    }

    public function contactarSend()
    {
        #dd(request());

        DB::table('mensagens')->insert([
            'razao_social' => Input::get('contact-razao'),
            'nome_fantasia' => Input::get('contact-nfantasia'),
            'cnpj' => Input::get('contact-cnpj'),
            'segmento' => Input::get('contact-segmento'),
            //'presidiario_id' => Input::get('p_id'),
            //'presidio_id' => Input::get('pres_id'),
            'ano_fundacao' => Input::get('contact-anofundacao'),
            'diretor_responsavel' => Input::get('contact-responsavel'),
            'tel' => Input::get('contact-tel'),
            'email' => Input::get('contact-email'),
            'endereco' => Input::get('contact-endereco'),
            'numero' => Input::get('contact-numero'),
            'bairro' => Input::get('contact-bairro'),
            'observacao' => Input::get('contact-observacao'),
        ]);

        request()->session()->flash('msg-success', ['Mensagem enviada com sucesso, aguarde nosso contato!']);

        return redirect()->route('site.index');
    }

}