<?php

namespace App\Http\Controllers;

use App\Http\Requests\PresidiarioRequest;
use App\Presidiario;
use App\Presidio;
use App\Profissao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PresidiarioController extends Controller
{
	/**
	 * @var Presidiario
	 */
	private $presidiario;
	private $user;
	/**
	 * @var Presidio
	 */
	private $presidio;
	/**
	 * @var Profissao
	 */
	private $profissao;

	public function __construct(Presidiario $presidiario, Presidio $presidio, Profissao $profissao)
	{
		$this->presidiario = $presidiario;

		$this->middleware(function ($request, $next) {
			$this->user= Auth::user();

			return $next($request);
		});
		$this->presidio = $presidio;
		$this->profissao = $profissao;
	}

	public function index(Request $request)
	{

	    $nome = '';
	    $prontuario_key = '';
		if($this->user->role == 1){
			#$presidiarios = $this->presidiario->all();
			$presidiarios = $this->presidiario/*->paginate(30)*/;
		} else {
			#$presidiarios = $this->presidiario->where('presidio_id', $this->user->presidio_id)->with('profissao')->get();
			$presidiarios = $this->presidiario->where('presidio_id', $this->user->presidio_id)->with('profissao');
		}

		if(Input::get('nome') != null){
		    $nome = Input::get('nome');
		    $presidiarios = $presidiarios->where('nome', 'like', '%' . $nome . '%');
        }
		if(Input::get('prontuario_key') != null){
		    $prontuario_key = Input::get('prontuario_key');
            $presidiarios = $presidiarios->where('prontuario_key', 'like', '%' . $prontuario_key . '%');
        }

        $presidiarios = $presidiarios->paginate(30);

		#dd($presidiarios);
		#dd(request()->input('page', 1));

		return view('adminlte::presidiarios.list', compact('presidiarios', 'nome', 'prontuario_key'))
            ->with([
                'i' => (request()->input('page', 1) - 1) * 5,
                'nome' => $nome,
                'prontuario_key' => $prontuario_key
            ]);
	}

	public function show($id)
	{
		$preso = $this->presidiario->get_preso_by_director($id, $this->user);

		if($preso){
			return view('adminlte::presidiarios.show', compact('preso'));
		} else {
			return redirect('adminlte::presidiarios.list', compact('preso'));
		}


	}

	public function create()
	{
		$presidios = $this->presidio->list_presidios_by_user_role($this->user);
		$profissoes = $this->profissao->all();

		return view('adminlte::presidiarios.create', compact('presidios', 'profissoes'));
	}

	public function store(PresidiarioRequest $presidiarioRequest)
	{
		$preso = Presidiario::create(Input::all());

		$preso->save();


		request()->session()->flash('msg-success', ['Presidiário cadastrado com sucesso']);
		return redirect()->route('presidiario.show', $preso->id);
	}

	public function delete($presidiario_id)
	{
		$presidiario = $this->presidiario->find($presidiario_id);
		$presidiario->delete();

		return redirect()->route('presidiario.list');
	}

	public function edit($id){

		$preso = $this->presidiario->find($id);

		if($preso->presidio_id != $this->user->presidio_id && $this->user->role != 1){
			request()->session()->flash('msg-infos', ['Você não tem permissão para editar este preso']);
			return redirect()->route('presidiario.list');
		}

		$presidios = $this->presidio->all();
		if($this->user->role != 1){
			$presidios = $this->presidio->where('id', $this->user->presidio_id)->get();
		}
		$profissoes = $this->profissao->all();

		return view('adminlte::presidiarios.edit', compact('preso', 'presidios', 'profissoes'));
	}


	public function update(PresidiarioRequest $presidiarioRequest, $id)
	{

		$preso = Presidiario::find($id);
		$inputUpdates = $presidiarioRequest->all();
		$inputUpdates = array_filter($inputUpdates); #remove os campos vazios

        #dd($inputUpdates);
		$preso->fill(array_filter($inputUpdates));

		if($preso->regime == 3){
		    $preso->presidio_id = 1;
        }
        if(isset($inputUpdates['status']) && $inputUpdates['status'] == '1'){
            $preso->status = true;
            #dd('ativo', $inputUpdates);
        } else {
            $preso->status = false;
            #dd('inativo', $inputUpdates);
        }
		#dd($preso);

		$preso->save();


		request()->session()->flash('msg-success', ['Presidiário atualizado com sucesso']);

		return redirect()->route('presidiario.list');
	}
}
