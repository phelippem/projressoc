<?php

namespace App\Http\Controllers;

use App\EvolucaoPresidiario;
use App\Presidiario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EvolucaoPresidiarioController extends Controller
{
	/**
	 * @var EvolucaoPresidiario
	 */
	private $evolucaoPresidiario;
	private $user;
	/**
	 * @var Presidiario
	 */
	private $presidiario;

	public function __construct(EvolucaoPresidiario $evolucaoPresidiario, Presidiario $presidiario)
	{

		$this->middleware(function ($request, $next) {
			$this->user= Auth::user();

			return $next($request);
		});

		$this->evolucaoPresidiario = $evolucaoPresidiario;
		$this->presidiario = $presidiario;
	}

	public function create($id_preso){

		$preso = $this->presidiario->get_preso_by_director($id_preso, $this->user);

		return view('adminlte::presidiario-evolucao.create', compact('preso'));
	}

	public function store(){

		$this->validate(request(), [
			'texto' => 'required',
			'conceito' => 'required',
			'presidiario_id' => 'required',
		]);

		#dd(request());
		$this->presidiario->addEvolucao(request());

		//dd($presidiario_id);
		return redirect()->route('presidiario.show',request()->presidiario_id);

	}
}
