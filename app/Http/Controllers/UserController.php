<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Presidio;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
	/**
	 * @var User
	 */
	private $user;
	/**
	 * @var Presidio
	 */
	private $presidio;

	public function __construct(Presidio $presidio)
	{
		$this->middleware(function ($request, $next) {
			$this->user= Auth::user();

			return $next($request);
		});

		$this->presidio = $presidio;
	}

	public function index(){
		$users = User::all();

		return view('adminlte::users.list', compact('users'));
	}

	public function show(){
		$user = $this->user;

		return view('adminlte::users.profile', compact('user'));
	}

	public function edit($id_user){

		if($this->user->role == 1 ){
			$usuario = User::find($id_user);
 		} else {
			#dd($id_user, $this->user->id);
			if($id_user != $this->user->id){ #rota com id diferente do id_user
				return redirect()->route('user.profile.edit', ['id' => $this->user->id]);
			}
			$usuario = User::find($this->user->id);
		}
		$presidios = $this->presidio->all();

		return view('adminlte::users.edit', compact('usuario', 'presidios'));
	}

	public function update(UserRequest $userRequest, $id)
	{

		$user = User::find($id);
		$inputUpdates = $userRequest->all();
		$inputUpdates = array_filter($inputUpdates); #remove os campos vazios

		$user->fill(array_filter($inputUpdates));
		$user->save();

		request()->session()->flash('msg-success', ['Usuário atualizado com sucesso']);
		return redirect()->route('user.profile.edit', $id);

	}

	public function create(){
		$usuario = new User();
		$presidios = $this->presidio->all();
		return view('adminlte::users.create', compact('usuario', 'presidios'));
	}

	public function store(UserRequest $userRequest)
	{

		$usuario = User::create(Input::all());

		$usuario->password = bcrypt(Input::get('password'));

		$usuario->save();

		request()->session()->flash('msg-success', ['Usuário cadastrado com sucesso']);
		return redirect()->route('user.profile.list');

	}
}
