<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MensagemController extends Controller
{


    public function __construct()
    {

    }

    public function index()
    {
        $mensagens = DB::table('mensagens')
            //->where('presidio_id', '=', Auth::user()->presidio_id)
            ->get();

        #dd($mensagens);

        return view('adminlte::mensagens.index', compact('mensagens'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function show($id)
    {
        $mensagem = DB::table('mensagens as m')
            ->select(['m.*', 'p.nome as p_nome', 'pr.nome as pr_nome', 'p.regime'])
            //->where('m.presidio_id', '=', Auth::user()->presidio_id)
            ->where('m.id', $id)
            ->leftJoin('presidiarios as p', 'm.presidiario_id', '=', 'p.id')
            ->leftJoin('presidios as pr', 'm.presidio_id', '=', 'pr.id')
            ->first();

        #dd($mensagem);

        return view('adminlte::mensagens.show', compact('mensagem'))->with('i', (request()->input('page', 1) - 1) * 5);
    }
}
