<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfissaoRequest;
use App\Profissao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfissaoController extends Controller
{
	/**
	 * @var Profissao
	 */
	private $profissao;

	public function __construct(Profissao $profissao)
	{
		$this->middleware(function ($request, $next) {
			$this->user= Auth::user();

			return $next($request);
		});
		$this->profissao = $profissao;
	}

	public function index()
	{
		$profissoes = $this->profissao->all();

		return view('adminlte::profissoes.list', compact('profissoes'));
	}

	public function create()
	{
		return view('adminlte::profissoes.create');
	}

	public function store(ProfissaoRequest $profissaoRequest)
	{
		$this->profissao->create([
			'nome' => $profissaoRequest->get('nome')
		]);

		request()->session()->flash('msg-success', ['Profissão cadastrado com sucesso']);

		return redirect()->route('profissao.list');
	}

	public function edit($id)
	{
		$profissao = $this->profissao->find($id);

		return view('adminlte::profissoes.edit', compact('profissao'));
	}


	public function update(ProfissaoRequest $profissaoRequest, $id)
	{

		$profissao = $this->profissao->find($id);

		$profissao->nome = request()->nome;
		$profissao->save();

		request()->session()->flash('msg-success', ['Profissão editada com sucesso']);

		return redirect()->route('profissao.list');
	}

	/*public function delete($id)
	{
		$profissao = $this->profissao->find($id);

		return view('adminlte::profissoes.edit', compact('profissao'));
	}*/
}
