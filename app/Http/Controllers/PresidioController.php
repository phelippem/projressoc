<?php

namespace App\Http\Controllers;

use App\Http\Requests\PresidioRequest;
use App\Presidio;
use Illuminate\Http\Request;

class PresidioController extends Controller
{
	/**
	 * @var Presidio
	 */
	private $presidio;

	public function __construct(Presidio $presidio)
	{

		$this->presidio = $presidio;
	}

	public function index()
	{
		$presidios = $this->presidio->all();

		return view('adminlte::presidios.list', compact('presidios'));
	}

	public function create()
	{
		return view('adminlte::presidios.create');
	}

	public function store(PresidioRequest $presidioRequest)
	{
		$this->presidio->create([
			'nome'=>$presidioRequest->get('nome')
		]);

		request()->session()->flash('msg-success', ['Presídio cadastrado com sucesso']);

		return redirect()->route('presidio.list');
	}

	/*public function delete($id)
	{
		$presidio = $this->presidio->find($id);
		$presidio->delete($id);

		request()->session()->flash('msg-success', ['Presídio deletado com sucesso']);

		return redirect()->route('presidio.list');
	}*/

	public function edit($id)
	{
		$presidio = $this->presidio->find($id);

		return view('adminlte::presidios.edit', compact('presidio'));
	}

	public function update(PresidioRequest $presidioRequest, $id)
	{
		#dd($id);
		$presidio = $this->presidio->find($id);
		$presidio->nome = $presidioRequest->get('nome');
		$presidio->save();

		request()->session()->flash('msg-success', ['Presídio atualizado com sucesso']);

		return redirect()->route('presidio.list');
	}
}
