<?php

namespace App\Http\Controllers;

use App\EvolucaoPresidiario;
use App\Presidio;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class RelatorioController extends Controller
{
    /**
     * @var Presidio
     */
    private $presidio;
    /**
     * @var EvolucaoPresidiario
     */
    private $evolucaoPresidiario;

    public function __construct(Presidio $presidio, EvolucaoPresidiario $evolucaoPresidiario)
    {
        $this->presidio = $presidio;
        $this->evolucaoPresidiario = $evolucaoPresidiario;
    }

    public function index(){
        $presidios = $this->presidio->all();

        return view('adminlte::relatorios.index', compact('presidios'));
    }

    public function results()
    {

        /*
         select `ep`.*, `p`.`nome` as `p_nome`, `pr`.`nome` as `pr_nome`, `p`.`regime`
        from `evolucao_presidiarios` as `ep`
        left join `presidiarios` as `p` on `ep`.`presidiario_id` = `p`.`id`
        left join `presidios` as `pr` on `p`.`presidio_id` = `pr`.`id`
        where `ep`.`conceito` = 2
        and `p`.`presidio_id` = 2
        group by `p`.`id`
         */

        $conceito = Input::get('conceito');
        $presidio_id = Input::get('presidio_id');
        $regime = Input::get('regime');
        $mes = Input::get('mes');
        $ano = Input::get('ano');

        $itens_per_page = 20;


        $rs = $this->evolucaoPresidiario
            ->select(['ep.*', 'p.nome as p_nome', 'pr.nome as pr_nome', 'p.regime'])
            ->from('evolucao_presidiarios as ep')
            ->leftJoin('presidiarios as p', 'ep.presidiario_id', '=', 'p.id')
            ->leftJoin('presidios as pr', 'p.presidio_id', '=', 'pr.id');

        if($conceito){
            $rs = $rs->where('ep.conceito', Input::get('conceito'));
        }
        if($presidio_id){
            $rs = $rs->where('p.presidio_id', $presidio_id);
        }
        if($regime){
            $rs = $rs->where('p.regime', $regime);
        }
        if($mes && $ano){
            $rs = $rs->whereMonth('ep.created_at', '=', $mes)
                ->whereYear('ep.created_at', '=', $ano);
        }

        $rs = $rs->groupBy('p.id');

        $total = $rs->get()->count();
        $last_page = (int) ceil($total / $itens_per_page);

        $rs = $rs->limit($itens_per_page);
        $rs = $rs->offset( (request()->input('page', 1)-1) * $itens_per_page );

        #dd($rs->toSql());
        #dd($rs->get());
        #$pags = new LengthAwarePaginator($rs, $rs->count(), $itens_per_page);
        #dd($rs->paginate());
        #$rs = $rs->paginate($itens_per_page);
        #$rs = $rs->get();
        #dd($rs);


        #dd($rs->paginate(\Config::get('constants.paginate_org_index')));
        #dd($rs->toSql());
        #dd($rs->toSql(), $rs->get());
        #dd($rs->get());

        $results = $rs->get();
        #$results = $rs->with('presidiario')->get();
        #$results = $rs->paginate(\Config::get('constants.paginate_org_index'));
        #$results = $rs;
        #dd($total, $last_page, $results);


        request();
        return view('adminlte::relatorios.list', compact('results', 'last_page'))
            #->with('i', (request()->input('page', 1) - 1) * 5, 'total', 'conceito', 'presidio_id', 'regime', 'mes', 'ano');
            ->with( 'page',  request()->except(['_method', '_token']));

    }
}
