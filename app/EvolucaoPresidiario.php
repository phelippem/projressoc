<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvolucaoPresidiario extends Model
{
	protected $table = 'evolucao_presidiarios';

	protected $fillable = [
		'texto', 'conceito', 'presidiario_id'
	];

	public function presidiario(){
		return $this->belongsTo(Presidiario::class);
	}
}
