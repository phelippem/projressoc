<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Presidio extends Model
{
	use SoftDeletes; #verificar trait

	protected $table = 'presidios';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'nome',
	];

	protected $dates = [
		'deleted_at'
	];

	public function presidiarios()
	{
		return $this->hasMany(Presidiario::class, 'presidio_id', 'id');
	}

	public function list_presidios_by_user_role($user)
	{
		if($user->role != 1) {
			$presidios = $this
				->where('id', $user->presidio_id)
				->get();
		} else {
			$presidios = $this->all();
		}

		#dd($presidios);

		return $presidios;
	}
}
