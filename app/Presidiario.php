<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Presidiario extends Model
{
	use SoftDeletes; #verificar trait

	protected $table = 'presidiarios';

	protected $fillable = [
		'nome', 'naturalidade', 'genero', 'data_nasc', 'profissao_id', 'regime', 'presidio_id', 'prontuario_key', 'perfil_profissional', 'status'
	];

	protected $dates = ['deleted_at'];

	public function presidio(){
		return $this->belongsTo(Presidio::class, 'presidio_id');
	}

	public function profissao(){
		return $this->belongsTo(Profissao::class, 'profissao_id', 'id');
	}

	public function evolucoes(){
		return $this->hasMany(EvolucaoPresidiario::class, 'presidiario_id');
	}

	// Helpers

	public function getAge() {

		$b_date = $this->data_nasc;
		#dd($b_date);
		$bd_carbon = Carbon::createFromFormat('d/m/Y', $b_date);
		#dd($bd_carbon);
		#dd($bd_carbon->diff(Carbon::now())->format('%y anos'));

		$age = $bd_carbon->diff(Carbon::now())->format('%y anos');

		return $age;
	}

	public function get_preso_by_director($id, $director)
	{
		/*$preso = DB::table('presidiarios')
			->select('presidiarios.*')
			->join('presidios', 'presidiarios.presidio_id', 'presidios.id')
			->join('users', 'presidios.id', 'users.presidio_id')
			->where('presidiarios.id', '=', $id)
			->where('users.id', '=', $director->id)
			->first();*/
		if($director->role != 1) {
			$preso = $this
				->where('presidio_id', $director->presidio_id)
				->where('id', $id)
				->first();
		} else {
			$preso = $this->where('id', $id)->first();
		}

		return $preso;
	}

	public function addEvolucao($data){

		EvolucaoPresidiario::create([
			'texto' => $data->texto,
			'conceito' => $data->conceito,
			'presidiario_id' => $data->presidiario_id,
		]);

	}

	public function setDataNascAttribute($value)
	{
		$this->attributes['data_nasc'] = Carbon::createFromFormat('d/m/Y', $value);
	}
	public function getDataNascAttribute()
	{
		$dt = Carbon::parse($this->attributes['data_nasc']);
		return $dt->format('d/m/Y');
	}

}
