<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::group(['as' => 'site.'], function () {
	Route::get('/', 'SiteController@index')->name('index');
	Route::get('/resultados', 'SiteController@resultados')->name('resultado');
	Route::get('/contactar', 'SiteController@contactar')->name('contactar');
	Route::post('/contactar', 'SiteController@contactarSend')->name('contactarSend');
});


/*Route::get('/entrar', 'CompaniesAuth\LoginController@showLoginForm')->name('login');
Route::post('/entrar', 'CompaniesAuth\LoginController@login')->name('login');
Route::post('/sair', 'CompaniesAuth\LoginController@logout')->name('logout');*/

	/*Route::group(['middleware' => 'companyCheckComplementData'], function () {
		Route::get('/painel', 'CompaniesController@home')->name('dashboard');
	});*/

Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

	//Route::get('home', 'HomeController@index')->name('home');

	Route::group(['prefix' => 'usuario', 'as' => 'user.'], function () {

		Route::group(['prefix' => 'perfil', 'as' => 'profile.'], function () {

			Route::get('/', 'UserController@show')->name('show');

			Route::get('editar/{id}', 'UserController@edit')->name('edit');
			Route::patch('editar/{id}', 'UserController@update')->name('update');

			Route::group(['middleware' => 'checkIfUserRoleAdmin'], function () {
				Route::get('listar', 'UserController@index')->name('list');
				Route::get('cadastrar', 'UserController@create')->name('create');
				Route::post('cadastrar', 'UserController@store')->name('store');

				/*Route::get('editar/{id}', 'UserController@edit')->name('edit');
				Route::patch('editar/{id}', 'UserController@update')->name('update');*/
			});
		});
	});



    Route::group(['prefix' => 'mensagem', 'as' => 'mensagem.', 'middleware' => 'checkIfUserRoleAdmin'], function () {
        Route::get('/', 'MensagemController@index')->name('index');
        Route::get('/{id}', 'MensagemController@show')->name('show');
    });

	Route::group(['prefix' => 'profissao', 'as' => 'profissao.', 'middleware' => 'checkIfUserRoleAdmin'], function () {
		Route::get('listar', 'ProfissaoController@index')->name('list');
		Route::get('cadastrar', 'ProfissaoController@create')->name('create');
		Route::post('cadastrar', 'ProfissaoController@store')->name('store');
		Route::get('{id}/editar', 'ProfissaoController@edit')->name('edit');
		Route::patch('{id}/editar', 'ProfissaoController@update')->name('update');
		Route::delete('{id}', 'ProfissaoController@delete')->name('delete');
	});

	Route::group(['prefix' => 'unidade', 'as' => 'presidio.', 'middleware' => 'checkIfUserRoleAdmin'], function () {
		Route::get('listar', 'PresidioController@index')->name('list');
		Route::get('cadastrar', 'PresidioController@create')->name('create');
		Route::post('cadastrar', 'PresidioController@store')->name('store');
		Route::get('{id}/editar', 'PresidioController@edit')->name('edit');
		Route::patch('{id}/editar', 'PresidioController@update')->name('update');
		Route::delete('{id}', 'PresidioController@delete')->name('delete');
	});

	Route::group(['prefix' => 'reeducando', 'as' => 'presidiario.'], function () {
		Route::get('listar', 'PresidiarioController@index')->name('list');
		Route::get('cadastrar', 'PresidiarioController@create')->name('create');
		Route::post('cadastrar', 'PresidiarioController@store')->name('store');
		Route::get('{id}', 'PresidiarioController@show')->name('show');
		Route::get('{id}/editar', 'PresidiarioController@edit')->name('edit');
		Route::patch('{id}/editar', 'PresidiarioController@update')->name('update');
		Route::delete('deletar/{id}', 'PresidiarioController@delete')->name('delete');


		Route::group(['prefix' => '{id_preso}/evolucao', 'as' => 'evolucao.'], function () {
			Route::get('cadastrar', 'EvolucaoPresidiarioController@create')->name('create');
			Route::post('cadastrar', 'EvolucaoPresidiarioController@store')->name('store');
		});
	});


    Route::group(['prefix' => 'relatorios', 'as' => 'relatorio.', 'middleware' => 'checkIfUserRoleAdmin'], function () {
        Route::get('/', 'RelatorioController@index')->name('index');
        Route::post('/resultados', 'RelatorioController@results')->name('result');
        Route::get('/resultados', 'RelatorioController@results')->name('result');
    });
    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});
