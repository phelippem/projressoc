//


/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./vendor/jquery.fancybox.min');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//$(document).foundation();

$(function() {

  $(".search input").on('keyup', function(){
    if( !$(this).val() ){
      $('.filtro').addClass('hide');
      //console.log('NÃO tem dado');
    } else {
      $('.filtro').removeClass('hide');
      //console.log('tem dado');
    }
  });

});