<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
	<label for="inp-nome">Nome: </label>
	<input name="nome" id="inp-nome" class="form-control" value="{{ isset($presidio->nome) ? (old('nome') ? old('nome') : $presidio->nome) : old('nome') }}" />
</div>