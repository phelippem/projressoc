@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<form action="{{route('presidiario.evolucao.store', $preso->id)}}" method="POST" class="form">
						<div class="box-header with-border">
							<h3 class="box-title">Cadastrar evolução para preso: <b>{{$preso->nome}}</b></h3>
						</div>
						<div class="box-body">

							{{ method_field('POST') }}
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="presidiario_id" value="{{ $preso->id }}">

							<div class="form-group">
								<label for="inp-conceito">Conceito: </label>
								<select class="form-control text-capitalize" id="inp-conceito" name="conceito">
									@for($i=1;$i<=4;$i++)
										<option value="{{$i}}">{{ config('helper.conceito_preso.'.$i) }}</option>
									@endfor
								</select>
							</div>
							<div class="form-group">
								<label for="inp-texto">Texto: </label>
								<textarea name="texto" id="inp-texto" class="form-control"></textarea>
							</div>
						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-success pull-right">Enviar</button>
						</div>
					</form>
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
