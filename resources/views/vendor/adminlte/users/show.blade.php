@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="">


				<!-- Default box -->
				<div class="box">
					@include('adminlte::layouts.error-messages')

					<div class="box-header">
						<h3 class="box-title"><b>Perfil do usuário </b></h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body box-profile">
						<h3 class="profile-username text-center">{{$user->name}}</h3>

						<p class="text-muted text-center">{{$user->presidio->nome}}</p>

						<ul class="list-group list-group-unbordered">
							<li class="list-group-item">
								<b>Followers</b> <a class="pull-right">1,322</a>
							</li>
							<li class="list-group-item">
								<b>Following</b> <a class="pull-right">543</a>
							</li>
							<li class="list-group-item">
								<b>Friends</b> <a class="pull-right">13,287</a>
							</li>
						</ul>

						<a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><b>Evolução</b></h3>
						<div class="box-tools pull-right">
							<a href="{{route('presidiario.evolucao.create', $preso->id)}}" class="btn btn-success">
								<i class="fa fw fa-plus"></i> <span class="hidden-xs hidden-sm hidden-md">Cadastrar evolução</span>
							</a>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Conceito</th>
									<th>Texto</th>
									<th>Data</th>
								</tr>
							</thead>
							<tbody>
								@foreach($preso->evolucoes as $evo)
									<tr>
										<td>{{config('helper.conceito_preso.'.$evo->conceito)}}</td>
										<td>{{$evo->texto}}</td>
										<td>{{Carbon\Carbon::parse($evo->created_at)->format('d-m-Y H:i:s')}}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->

					<div class="box-footer text-right">
						<a href="{{route('presidiario.evolucao.create', $preso->id)}}" class="btn btn-success">
							<i class="fa fw fa-plus"></i> <span class="hidden-xs hidden-sm hidden-md">Cadastrar evolução</span>
						</a>
					</div>
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
