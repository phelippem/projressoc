<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
	<label for="inp-nome">Nome: </label>
	<input name="name" id="inp-nome" class="form-control" value="{{ isset($usuario->name) ? (old('nome') ? old('nome') : $usuario->name) : old('nome') }}" />
</div>
<div class="form-group">
	<label for="inp-email">E-mail: </label>
	<input name="email" id="inp-email" class="form-control" value="{{ isset($usuario->email) ? (old('email') ? old('email') : $usuario->email) : old('email') }}" />
</div>
<div class="form-group">
	<label for="inp-senha">Senha: <small>(min. 8 caracteres)</small></label>
	<input name="password" id="inp-senha" class="form-control" type="password" value="" />
</div>
<div class="form-group">
	<label for="inp-csenha">Confirmação de senha: <small>(min. 8 caracteres)</small></label>
	<input name="password_confirmation" id="inp-csenha" class="form-control" type="password" value="" />
</div>

<div class="form-group">
	<label for="inp-tipo">Tipo de usuário: </label>
	{{--<input id="inp-tipo" class="form-control text-capitalize" name="role" value="{{ config("helper.roles.$usuario->role") }}" />--}}

{{--{{dd($user->role)}}--}}
	<select class="form-control text-capitalize" id="inp-role" {{$user->role == 1 ? '' : 'disabled' }} name="role">
		@for($i=1 ; $i<=2 ; $i++)
			<option value="{{$i}}" {{ isset($usuario) ? ($usuario->role == $i ? 'selected' : '' ) : '' }} >
				{{ config("helper.roles.".$i) }}
			</option>
		@endfor
	</select>

</div>

@if($user->role == 1)
	<div class="form-group">
		<label for="inp-presidio">Presídio: </label>
		<select class="form-control text-capitalize" id="inp-presidio" name="presidio_id">
			<option value="">Selecione </option>
			@foreach($presidios as $p)
				<option value="{{$p->id}}" {{ isset($usuario) ? ($usuario->presidio_id == $p->id ? 'selected' : '' ) : '' }}>{{ $p->nome }}</option>
			@endforeach
		</select>
	</div>
@endif