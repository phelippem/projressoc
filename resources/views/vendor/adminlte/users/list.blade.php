@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Total de usuários do sistema: <b>{{count($users)}}</b></h3>

						<div class="box-tools pull-right">
							<a href="{{route('user.profile.create')}}" class="btn btn-success">
								<i class="fa fw fa-plus"></i> <span class="hidden-xs hidden-sm hidden-md">Cadastrar usuário</span>
							</a>
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body table-responsive">

						@include('adminlte::layouts.flash-messages')

						<table id="" class="table data-table table-hover" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Nome</th>
									<th>Presídio</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Nome</th>
									<th>Presídio</th>
									<th>Ações</th>
								</tr>
							</tfoot>
							<tbody>
							@foreach($users as $u)
								{{--{{ dd($p->data_nasc) }}--}}
								<tr>
									<td class="text-capitalize">{{$u->name}}</td>
									<td class="text-capitalize">{{$u->presidio->nome}}</td>
									<td>
										<a href="{{ route('user.profile.edit', $u->id) }}" class="btn btn-warning"><i class="fa fw fa-pencil"></i></a>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
					<div class="box-footer">
						<div class="pull-right">
							<a href="{{route('user.profile.create')}}" class="btn btn-success">
								<i class="fa fw fa-plus"></i> <span class="hidden-xs hidden-sm hidden-md">Cadastrar usuário</span>
							</a>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
