@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">

				<!-- Default box -->
				<div class="box">

					<div class="box-header">
						<h3 class="box-title">Perfil do usuário</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body box-profile">
						@include('adminlte::layouts.flash-messages')
						<h3 class="profile-username text-center">{{$user->name}}</h3>

						<ul class="list-group list-group-unbordered">
							<li class="list-group-item">
								<b>Presídio: </b> <a class="pull-right">{{$user->presidio->nome}}</a>
							</li>
							<li class="list-group-item">
								<b>E-mail: </b> <a class="pull-right">{{$user->email}}</a>
							</li>
						</ul>

						<a href="{{route('user.profile.edit', $user->id)}}" class="btn btn-primary btn-block"><b><i class="fa fw fa-pencil"></i> Editar</b></a>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
