<ul class="pagination">
	{{--{{dd(request()->all())}}--}}
	{{--{{   dd(URL::to('relatorio.result', array('id'=>'abf') ) )  }}--}}
	{{--{{dd( route('relatorio.result', request()->all()) )}}--}}
	{{--{{dd( request()->merge(['page', $page-1])) }}--}}
	{{-- Previous Page Link --}}
	{{--{{ request()->replace([request()->all(), 'page' => $page-1]) }}--}}
	{{--{{dd(request()->all())}}--}}
	@if (!isset($page) || $page == 1)
		<li class="disabled"><span>&laquo;</span></li>
	@else
		{{ request()->merge(['page' => ($page-1)]) }}
		<li><a href="{{ route('relatorio.result', request()->all() ) }}" rel="prev">&laquo;</a></li>
	@endif

	{{--{{dd('asd')}}--}}
	{{-- Pagination Elements --}}
	@for ($i=1 ; $i<=$last_page ; $i++ )
		{{-- "Three Dots" Separator --}}
		{{--@if (is_string($element))
			<li class="disabled"><span>{{ $element }}</span></li>
		@endif--}}

		{{-- Array Of Links --}}
		{{--@if (is_array($element))
			@foreach ($element as $page => $url)
				@if ($page == $paginator->currentPage())
					<li class="active"><span>{{ $page }}</span></li>
				@else
					<li><a href="{{ $url }}">{{ $page }}</a></li>
				@endif
			@endforeach
		@endif--}}

		@if ($page == $i)
			<li class="active"><span>{{ $i }}</span></li>
		@else
			{{ request()->merge(['page' => ($i)]) }}
			<li><a href="{{ route('relatorio.result', request()->all() ) }}">{{ $i }}</a></li>
		@endif
	@endfor

	{{-- Next Page Link --}}
	@if ($i-1 != $last_page)
		{{ request()->merge(['page' => $page+1]) }}
		<li><a href="{{ route('relatorio.result', request()->all() ) }}" rel="next">&raquo;</a></li>
	@else
		<li class="disabled"><span>&raquo;</span></li>
	@endif
</ul>
