@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Total encontrado: <b>{{count($results)}}</b></h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body table-responsive">

						@include('adminlte::layouts.flash-messages')

						<table id="" class="table data-table table-hover" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Nome</th>
									<th>Regime</th>
									<th>Presídio</th>
									<th>Sexo</th>
									<th>Conceito / Data</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Nome</th>
									<th>Regime</th>
									<th>Presídio</th>
									<th>Sexo</th>
									<th>Conceito / Data</th>
									<th>Ações</th>
								</tr>
							</tfoot>
							<tbody>
							{{--{{dd($results)}}--}}
							@foreach($results as $p)
								{{--{{ dd($p) }}--}}
								{{--{{ dd($p->created_at->format('d/m/Y')) }}--}}
								<tr>
									<td class="text-capitalize">{{$p->presidiario->nome}}</td>
									<td class="text-capitalize">{{config('helper.regimes.'.$p->presidiario->regime)}}</td>
									<td class="text-capitalize">{{$p->presidiario->presidio->nome}}</td>
									<td class="text-capitalize">{{config('helper.genero.'.$p->presidiario->genero)}}</td>
									<td class="text-capitalize">{{config('helper.conceito_preso.'.$p->conceito)}} ({{$p->created_at->format('d/m/Y')}})</td>
									<td>
										<a href="{{ route('presidiario.show', $p->presidiario->id) }}" class="btn btn-info pull-left"><i class="fa fw fa-eye"></i></a>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						{{--{{ $results->render('adminlte::relatorios.pagination', ['page'=>request('page', 1), 'last_page'=>'last_page']) }}--}}
						{{--{{dd($page)}}--}}
						@include('adminlte::relatorios.pagination', array(
							'page' => request('page', 1),
							'last_page' => $last_page,
						))
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
