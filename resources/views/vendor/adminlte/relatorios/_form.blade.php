{{--<input type="hidden" name="_token" value="{{ csrf_token() }}">--}}

<div class="form-group col-lg-4">
	<label for="inp-regime">Regime: </label>
	<select class="form-control text-capitalize" id="inp-regime" name="regime">
		<option value="">Selecione </option>
		@foreach(config('helper.regimes') as $k=>$regime)
			<option value="{{$k}}" >{{ $regime  }}</option>
		@endforeach

	</select>
</div>

<div class="form-group col-lg-4">
	<label for="inp-presidio">Presidio: </label>
	<select class="form-control text-capitalize" id="inp-presidio" name="presidio_id">
		<option value="">Selecione </option>
		@foreach($presidios as $p)
			<option value="{{$p->id}}" >{{ $p->nome }}</option>
		@endforeach
	</select>
</div>

<div class="form-group col-lg-4">
	<label for="inp-conceito">Conceito: </label>
	<select class="form-control text-capitalize" id="inp-conceito" name="conceito">
		<option value="">Selecione </option>
		@foreach(config('helper.conceito_preso') as $k=>$conceito)
			<option value="{{$k}}" >{{ $conceito }}({{$k}})</option>
		@endforeach
	</select>
</div>

<div class="form-group col-lg-4">
	<label for="inp-mes">Mês: </label>
	<select class="form-control text-capitalize" id="inp-mes" name="mes">
		<option value="">Selecione </option>
		<option value="1" >Janeiro</option>
		<option value="2" >Fevereiro</option>
		<option value="3" >Março</option>
		<option value="4" >Abril</option>
		<option value="5" >Maio</option>
		<option value="6" >Junho</option>
		<option value="7" >Julho</option>
		<option value="8" >Agosto</option>
		<option value="9" >Setembro</option>
		<option value="10" >Outubro</option>
		<option value="11" >Novembro</option>
		<option value="12" >Dezembro</option>
	</select>
</div>

<div class="form-group col-lg-4">
	<label for="inp-ano">Ano: </label>
	<input name="ano" id="inp-ano" class="form-control"  />
</div>
