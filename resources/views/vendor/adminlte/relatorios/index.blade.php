@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="">

				<!-- Default box -->
				<div class="box">
					<form action="{{route('relatorio.result')}}" method="GET" class="form">
						<div class="box-header with-border">
							<h3 class="box-title">Cadastrar reeducando</h3>
						</div>
						<div class="box-body">
							@include('adminlte::layouts.flash-messages')

							{{--{{ method_field('GET') }}--}}

							@include('adminlte::relatorios._form')
						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-success pull-right">Enviar</button>
						</div>
					</form>
				</div>

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title"><b>Reltórios</b></h3>

						<div class="box-tools pull-right">

						</div>
					</div>
					<div class="box-body table-responsive">

						@include('adminlte::layouts.flash-messages')

						<table id="" class="table data-table table-hover" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Nome</th>
									<th>Regime</th>
									<th>Profissão</th>
									<th>Idade</th>
									<th>Sexo</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Nome</th>
									<th>Regime</th>
									<th>Profissão</th>
									<th>Idade</th>
									<th>Sexo</th>
									<th>Ações</th>
								</tr>
							</tfoot>
							<tbody>
							{{--@foreach($presidiarios as $p)
								<tr>
									<td class="text-capitalize">{{$p->nome}}</td>
									<td class="text-capitalize">{{config('helper.regimes.'.$p->regime)}}</td>
									<td class="text-capitalize">{{$p->profissao->nome}}</td>
									<td class="text-capitalize">{{$p->getAge()}}</td>
									<td class="text-capitalize">{{config('helper.genero.'.$p->genero)}}</td>
									<td>
										<a href="{{ route('presidiario.show', $p->id) }}" class="btn btn-info pull-left"><i class="fa fw fa-eye"></i></a>
										<a href="{{ route('presidiario.edit', $p->id) }}" class="btn btn-warning pull-left"><i class="fa fw fa-pencil"></i></a>
										<form action="{{ route('presidiario.delete', $p->id) }}" method="POST" class="pull-left" >
											<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
											<input type="hidden" name="_method" value="DELETE" />
											<button type="submit" class="btn btn-danger pull-left" title="Deletar" ><i class="fa fa-remove"></i> </button>
										</form>
									</td>
								</tr>
							@endforeach --}}
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
