@if(count($errors))
    <div class="form-group">
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
{{--//https://github.com/laracasts/flash--}}
{{--{{dd(request()->session('messages'))}}--}}

@if(session('msg-success'))
    <div class="form-group">
        <div class="alert alert-success">
            <ul>
                @foreach(session('msg-success') as $message)
                    <li>{{$message}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

@if(session('msg-infos'))
    <div class="form-group">
        <div class="alert alert-warning">
            <ul>
                @foreach(session('msg-infos') as $message)
                    <li>{{$message}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
