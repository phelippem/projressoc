<!-- Main Footer -->
<footer class="main-footer">
    <!-- Default to the left -->
    <strong>Copyright &copy; {{date('Y')}}.</strong> {{ trans('adminlte_lang::message.createdby') }} <a href="https://fb.com/phelippem" > <b>Phelippe Matte da Fonseca</b></a>.
    <div class="pull-right">
        <a href="mailto:contato@phelippe.com"><b>Contato</b></a>
    </div>
</footer>