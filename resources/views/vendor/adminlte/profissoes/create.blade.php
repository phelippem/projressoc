@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<form action="{{route('profissao.store')}}" method="POST" class="form">
						<div class="box-header with-border">
							<h3 class="box-title">Cadastrar profissão</h3>
						</div>
						<div class="box-body">
							@include('adminlte::layouts.error-messages')

							{{ method_field('POST') }}

							@include('adminlte::profissoes._form')

						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-success pull-right">Enviar</button>
						</div>
					</form>
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
