@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Total de profissões: <b>{{count($profissoes)}}</b></h3>

						<div class="box-tools pull-right">
							<a href="{{route('profissao.create')}}" class="btn btn-success">
								<i class="fa fw fa-plus"></i> <span class="hidden-xs hidden-sm hidden-md">Cadastrar profissão</span>
							</a>
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body table-responsive">

						<table id="" class="table data-table table-hover" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Nome</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Nome</th>
									<th>Ações</th>
								</tr>
							</tfoot>
							<tbody>
							@foreach($profissoes as $p)
								{{--{{ dd($p->data_nasc) }}--}}
								<tr>
									<td class="text-capitalize">{{$p->nome}}</td>
									<td>
										<a href="{{ route('profissao.edit', $p->id) }}" class="btn btn-warning pull-left"><i class="fa fw fa-pencil"></i></a>
										{{--<form action="{{ route('profissao.delete', $p->id) }}" method="POST" class="pull-left" >
											<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
											<input type="hidden" name="_method" value="DELETE" />
											<button type="submit" class="btn btn-danger" title="Deletar" ><i class="fa fa-remove"></i> </button>
										</form>--}}
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
					<div class="box-footer">
						<div class="pull-right">
							<a href="{{route('profissao.create')}}" class="btn btn-success">
								<i class="fa fw fa-plus"></i> <span class="hidden-xs hidden-sm hidden-md">Cadastrar profissão</span>
							</a>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
