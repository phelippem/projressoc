@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="">

				<!-- Default box -->
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><b>Reeducando: </b>{{$mensagem->p_nome}}</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover">
							<tbody>
							<tr>
								<td><b>Razão social</b></td>
								<td class="text-capitalize">{{$mensagem->razao_social}}</td>
							</tr>
							<tr>
								<td><b>Nome fantasia</b></td>
								<td class="text-capitalize">{{$mensagem->nome_fantasia}}</td>
							</tr>
							<tr>
								<td><b>CNPJ</b></td>
								<td class="text-capitalize">{{$mensagem->cnpj}}</td>
							</tr>
							<tr>
								<td><b>Segmento</b></td>
								<td class="text-capitalize">{{$mensagem->segmento}}</td>
							</tr>
							<tr>
								<td><b>Ano Fundação</b></td>
								<td class="text-capitalize">{{$mensagem->ano_fundacao}}</td>
							</tr>
							<tr>
								<td><b>Diretor Responsável</b></td>
								<td class="text-capitalize">{{$mensagem->diretor_responsavel}}</td>
							</tr>
							<tr>
								<td><b>Telefone</b></td>
								<td class="text-capitalize">{{$mensagem->tel}}</td>
							</tr>
							<tr>
								<td><b>E-mail</b></td>
								<td class="text-capitalize">{{$mensagem->email}}</td>
							</tr>
							<tr>
								<td><b>Endereço</b></td>
								<td class="text-capitalize">{{$mensagem->endereco}}</td>
							</tr>
							<tr>
								<td><b>Número</b></td>
								<td class="text-capitalize">{{$mensagem->numero}}</td>
							</tr>
							<tr>
								<td><b>Bairro</b></td>
								<td class="text-capitalize">{{$mensagem->bairro}}</td>
							</tr>
							<tr>
								<td><b>Observação</b></td>
								<td class="text-capitalize">{{$mensagem->observacao}}</td>
							</tr>
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
