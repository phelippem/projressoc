@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="">

				{{--{{dd($mensagens)}}--}}
				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Total de mensagens: <b>{{count($mensagens)}}</b></h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body table-responsive">

						<table id="" class="table data-table table-hover" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Nome Fantasia</th>
									<th>CNPJ</th>
									<th>E-mail</th>
									<th>Telefone</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Nome Fantasia</th>
									<th>CNPJ</th>
									<th>E-mail</th>
									<th>Telefone</th>
									<th>Ações</th>
								</tr>
							</tfoot>
							<tbody>
							@foreach($mensagens as $m)
								{{--{{ dd($p->data_nasc) }}--}}
								<tr>
									<td class="text-capitalize">{{$m->nome_fantasia}}</td>
									<td class="text-capitalize">{{$m->cnpj}}</td>
									<td class="text-capitalize">{{$m->email}}</td>
									<td class="text-capitalize">{{$m->tel}}</td>
									<td>
										<a href="{{ route('mensagem.show', $m->id) }}" class="btn btn-info pull-left"><i class="fa fw fa-eye"></i></a>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
