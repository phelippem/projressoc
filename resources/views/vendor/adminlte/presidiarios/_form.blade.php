<input type="hidden" name="_token" value="{{ csrf_token() }}">

<div class="form-group">
	<label for="inp-regime">Regime: </label>
	<select class="form-control text-capitalize" id="inp-regime" name="regime">
		<option value="">Selecione </option>
		@foreach(config('helper.regimes') as $k=>$regime)
			<option value="{{$k}}" {{ ( isset($preso) ? ( $preso->regime == $k ? 'selected' : '' ) : '' )}} >{{ $regime  }}</option>
		@endforeach

	</select>
</div>

<div class="form-group">
	<label for="inp-presidio">Presidio: </label>
	<select class="form-control text-capitalize" id="inp-presidio" name="presidio_id">
		<option value="">Selecione </option>
		@foreach($presidios as $p)
			<option value="{{$p->id}}" {{ (isset($preso) ? ( $preso->presidio_id == $p->id ? 'selected' : 'not' ) : '' ) }}>{{ $p->nome }}</option>
		@endforeach
	</select>
</div>
<div class="form-group">
	<label for="inp-nome">Nome: </label>
	<input name="nome" id="inp-nome" class="form-control" value="{{ isset($preso) ? ( old('nome') ? old('nome') : $preso->nome) : old('nome') }}" />
</div>
<div class="form-group">
	<label for="inp-datanasc">Data de nascimento: </label>
	<input name="data_nasc" id="inp-datanasc" class="form-control" value="{{ isset($preso) ? ( old('data_nasc') ? old('data_nasc') : $preso->data_nasc) : old('data_nasc') }}" />
</div>
<div class="form-group">
	<label for="inp-naturalidade">Naturalidade: </label>
	<input name="naturalidade" id="inp-naturalidade" class="form-control" value="{{ isset($preso) ? ( old('naturalidade') ? old('naturalidade') : $preso->naturalidade) : old('naturalidade') }}" />
</div>
<div class="form-group">
	<label for="inp-genero">Gênero: </label>
	<select class="form-control text-capitalize" id="inp-genero" name="genero">
		<option value="1" {{ (isset($preso) ? $preso->genero == 1 ? 'selected' : ''  : '' )}}>{{ config('helper.genero.1') }}</option>
		<option value="2" {{ (isset($preso) ? $preso->genero == 2 ? 'selected' : ''  : '' )}}>{{ config('helper.genero.2') }}</option>
	</select>
</div>
<div class="form-group">
	<label for="inp-prontuario">Prontuário: </label>
	<input name="prontuario_key" id="inp-prontuario" class="form-control" value="{{ isset($preso) ? ( old('prontuario_key') ? old('prontuario_key') : $preso->prontuario_key) : old('prontuario_key') }}" />
</div>

<div class="form-group">
	<label for="inp-profissao">Profissão: </label>
	<select class="form-control text-capitalize" id="inp-profissao" name="profissao_id">
		<option value="">Selecione a profissão:</option>
		@foreach($profissoes as $pr)
			<option value="{{$pr->id}}" {{ (isset($preso) ? ( $preso->profissao_id == $pr->id ? 'selected' : 'not' ) : '' ) }}>{{ $pr->nome }}</option>
		@endforeach
	</select>
</div>

<div class="form-group">
	<label for="inp-profissao">Status: </label>
	<select class="form-control text-capitalize" id="inp-status" name="status">
		<option value="0" {{ (isset($preso) ? $preso->status == 0 ? 'selected' : ''  : '' )}}>{{ config('helper.status.0') }}</option>
		<option value="1" {{ (isset($preso) ? $preso->status == 1 ? 'selected' : ''  : '' )}}>{{ config('helper.status.1') }}</option>
	</select>
</div>

<div class="form-group">
	<label for="inp-perfil">Perfil profissional: </label>
	<textarea name="perfil_profissional" id="inp-perfil" class="form-control" >{{ isset($preso) ? ( old('perfil_profissional') ? old('perfil_profissional') : $preso->perfil_profissional) : old('perfil_profissional') }}</textarea>
</div>
