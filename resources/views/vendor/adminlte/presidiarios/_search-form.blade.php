<input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group">
	<label for="inp-nome">Nome: </label>
	<input name="nome" id="inp-nome" class="form-control" value="{{ isset($nome) ? $nome : old('nome') }}" />
</div>
<div class="form-group">
	<label for="inp-prontuario">Prontuário: </label>
	<input name="prontuario_key" id="inp-prontuario" class="form-control" value="{{ isset($prontuario_key) ? $prontuario_key : old('prontuario_key') }}" />
</div>