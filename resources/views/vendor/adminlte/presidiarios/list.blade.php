@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="">

				<div class="box box-search">
					<div class="box-header with-border">
						<form action="{{route('presidiario.list')}}" method="GET" class="form">
							<div class="box-header with-border">
								<h4 class="box-title">Buscar reeducando:</h4>
							</div>
							<div class="box-body">
								{{ method_field('GET') }}

								@include('adminlte::presidiarios._search-form')
							</div>
							<!-- /.box-body -->
							<div class="box-footer">
								<button type="submit" class="btn btn-success pull-right">Buscar</button>
								<a href="{{route('presidiario.list')}}" class="btn btn-warning ">Limpar busca</a>
							</div>
						</form>
					</div>
				</div>

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Total de reeducandos: <b>{{count($presidiarios)}}</b></h3>

						<div class="box-tools pull-right">
							<a href="{{route('presidiario.create')}}" class="btn btn-success">
								<i class="fa fw fa-plus"></i> <span class="hidden-xs hidden-sm hidden-md">Cadastrar reeducando</span>
							</a>
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body table-responsive">

						@include('adminlte::layouts.flash-messages')

						<table id="" class="table data-table table-hover" cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Nome</th>
									<th>Regime</th>
									<th>Profissão</th>
									<th>Idade</th>
									<th>Sexo</th>
									<th>Status</th>
									<th>Ações</th>
								</tr>
							</thead>
							<tfoot>
								<tr>
									<th>Nome</th>
									<th>Regime</th>
									<th>Profissão</th>
									<th>Idade</th>
									<th>Sexo</th>
									<th>Status</th>
									<th>Ações</th>
								</tr>
							</tfoot>
							<tbody>
							@foreach($presidiarios as $p)
								{{--{{ dd($p->data_nasc) }}--}}
								<tr>
									<td class="text-capitalize">{{$p->nome}}</td>
									<td class="text-capitalize">{{config('helper.regimes.'.$p->regime)}}</td>
									<td class="text-capitalize">{{$p->profissao->nome}}</td>
									<td class="text-capitalize">{{$p->getAge()}}</td>
									<td class="text-capitalize">{{config('helper.genero.'.$p->genero)}}</td>
									<td class="text-capitalize">{{config('helper.status.'.$p->status)}}</td>
									<td>
										<a href="{{ route('presidiario.show', $p->id) }}" title="Exibir" class="btn btn-info pull-left"><i class="fa fw fa-eye"></i></a>
										<a href="{{ route('presidiario.edit', $p->id) }}" title="Editar" class="btn btn-warning pull-left"><i class="fa fw fa-pencil"></i></a>
										<form action="{{ route('presidiario.delete', $p->id) }}" method="POST" class="pull-left" >
											<input type="hidden" name="_token" value="{{ csrf_token() }}"/>
											<input type="hidden" name="_method" value="DELETE" />
											<button type="submit" class="btn btn-danger pull-left" title="Deletar" ><i class="fa fa-remove"></i> </button>
										</form>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						{!! $presidiarios->appends(['prontuario_key'=>$prontuario_key, 'nome'=>$nome])->links() !!}
					</div>
					<!-- /.box-body -->
					<div class="box-footer with-border text-right">
						<a href="{{route('presidiario.create')}}" class="btn btn-success">
							<i class="fa fw fa-plus"></i> <span class="hidden-xs hidden-sm hidden-md">Cadastrar reeducando</span>
						</a>
					</div>
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
