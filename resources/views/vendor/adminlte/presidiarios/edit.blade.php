@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<form action="{{route('presidiario.edit', $preso->id) }}" method="POST" class="form">
						<div class="box-header with-border">
							<h3 class="box-title">Editar presidiário</h3>
						</div>
						<div class="box-body">
							@include('adminlte::layouts.flash-messages')

							{{ method_field('PATCH') }}

							@include('adminlte::presidiarios._form')

						</div>
						<!-- /.box-body -->
						<div class="box-footer">
							<button type="submit" class="btn btn-success pull-right"><i class="fa fw fa-pencil"></i> Editar</button>
						</div>
					</form>
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection

@section('post-scripts')
	<script type="text/javascript">

        var inp_presidio = $('#inp-presidio');
        inp_presidio.attr('disabled', true).parent().hide();

        $('#inp-regime').on('change', function(){
            var regime = $(this).val();
            if(regime != 3){
                inp_presidio.attr('disabled', false).parent().show()
            } else {
                inp_presidio.attr('disabled', true).parent().hide();
            }
        });
	</script>
@endsection