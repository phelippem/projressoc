@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection

@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="">

				<!-- Default box -->
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><b>Reeducando: </b>{{$preso->nome}}</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover">
							<tbody>
								<tr>
									<td><b>Nome</b></td>
									<td class="text-capitalize">{{$preso->nome}}</td>
								</tr>
								<tr>
									<td><b>Naturalidade</b></td>
									<td class="text-capitalize">{{$preso->naturalidade}}</td>
								</tr>
								<tr>
									<td><b>Gênero</b></td>
									<td class="text-capitalize">{{config('helper.genero.'.$preso->genero)}}</td>
								</tr>
								<tr>
									<td><b>Data nascimento / Idade</b></td>
									<td class="text-capitalize"> {{ $preso->data_nasc }} ({{$preso->getAge()}})</td>
								</tr>
								<tr>
									<td><b>Regime</b></td>
									<td class="text-capitalize">{{config('helper.regimes.'.$preso->regime)}}</td>
								</tr>
								<tr>
									<td><b>Profissão</b></td>
									<td class="text-capitalize">{{$preso->profissao->nome}}</td>
								</tr>
								<tr>
									<td><b>Prontuário</b></td>
									<td>{{$preso->prontuario_key}}</td>
								</tr>
								<tr>
									<td><b>Perfil profissional</b></td>
									<td>{{$preso->perfil_profissional}}</td>
								</tr>
								<tr>
									<td><b>Status</b></td>
									<td class="text-capitalize">{{config('helper.status.'.$preso->status)}}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
				<div class="box">
					<div class="box-header">
						<h3 class="box-title"><b>Evolução</b></h3>
						<div class="box-tools pull-right">
							<a href="{{route('presidiario.evolucao.create', $preso->id)}}" class="btn btn-success">
								<i class="fa fw fa-plus"></i> <span class="hidden-xs hidden-sm hidden-md">Cadastrar evolução</span>
							</a>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Conceito</th>
									<th>Texto</th>
									<th>Data</th>
								</tr>
							</thead>
							<tbody>
								@foreach($preso->evolucoes as $evo)
									<tr>
										<td>{{config('helper.conceito_preso.'.$evo->conceito)}}</td>
										<td>{{$evo->texto}}</td>
										<td>{{Carbon\Carbon::parse($evo->created_at)->format('d-m-Y H:i:s')}}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<!-- /.box-body -->

					<div class="box-footer text-right">
						<a href="{{route('presidiario.evolucao.create', $preso->id)}}" class="btn btn-success">
							<i class="fa fw fa-plus"></i> <span class="hidden-xs hidden-sm hidden-md">Cadastrar evolução</span>
						</a>
					</div>
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
