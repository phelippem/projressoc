<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en" class="no-js" dir="ltr">

@section('head')
    @include('site.site-head')
@show

  <body>
    <div class="fullwight row">
      <img src="images/site/topo.jpg" class="first_img">
        <div class="large columns seres large-centered">
          <center><a href="/"><img src="images/site/seres.png"></a><br><br>
          <span>Programa de Reinserção Social do Sistema Prisional<br>
                    do Estado de Pernambuco</span><br><br>
              <a data-fancybox data-src="#hidden-content" data-fancybox data-width="80%" data-src="#hidden-content" href="javascript:;" class="button">ENTRE EM CONTATO</a>
              <div style="display: none;" id="hidden-content">
                  <h4><center>Contactar Trabalhador</center></h4>
                  <form action="{{route('site.contactar')}}" method="post">
                      @include('site._form-contactar')
                  </form>
              </div>
          </center>
        </div>
    </div>
    <div style="display: none; width: 60%;" id="hidden-content-2">
      <h4><center>Sistema de Reinserção Social do Estado de Pernambuco SIRESPE</center></h4>
<p>O Programa de Reinserção Social do Estado de Pernambuco tem como objetivo unir esforços Governamentais e  Empresariais com o fim de relocar no mercado de trabalho cidadãos apenados. O SIRESPE contará com um banco de atribuições profissionais e  avaliações periódicas que facilitarão a aproximação entre empresas e apenados. Nos regimes aberto e semi-aberto o apenado se dirigirá ao local de trabalho externo, no caso do regime fechado o Empresário fará uma bancada de trabalho no estabelecimento prisional. O Empresário que desejar contratar, após consulta ao sistema, bastará contactar à Secretaria de Ressocialização, no caso de apenados nos regimes fechado e semi-fechado e  Patronato no regime aberto.</p>
    </div>
    <div class="row">
        <div class="conteudo columns large-12">

            <div class="row beneficios">
                <div class="large-10 columns large-centered" style="margin-top: 30px;">
                    @include('site.flash-messages')
                </div>
              @include('site._form')
            </div>
            <div class="row beneficios">
                <div class="row">
                    <div class="large-10 columns large-centered">
                        <div class="large-6 columns">
                            <h2>Benefícios à Sociedade</h2>
                            <p>O ganho econômico e social de mais um cidadão integrante ao mercado de trabalho é positivamente imensurável. Além de  reduzir a probabilidade do apenado  reincidir criminalmente. O trabalho é dever social, dignifica o homem e tem finalidade educativa e produtiva.</p>
                        </div>
                        <div class="large-6 columns">
                            <img src="images/site/sociedade.png">
                        </div>
                    </div>
                </div><br><br>
                <div class="row">
                    <div class="large-10 columns large-centered">
                        <div class="large-6 columns">
                            <img src="images/site/empregador.png">
                        </div>
                        <div class="large-6 columns">
                            <h2>Benefícios ao empregador</h2>
                            <p>Conforme o art 28 § 2º,da lei das execuções penais, O trabalho do preso não está sujeito ao regime da Consolidação das Leis do Trabalho, logo o empregador  não arcará com as obrigações sociais. Terá ainda como  vantagem a possibilidade de remunerar o reeducando com o valor de  ¾ do salário mínimo vigente.</p>
                        </div>
                    </div>
                </div><br><br>
                <div class="row">
                    <div class="large-10 columns large-centered">
                        <div class="large-6 columns">
                            <h2>Benefícios ao reeducando</h2>
                            <p>No regime fechado e semi-aberto o reeducando poderá ter sua pena reduzida mediante o trabalho regular à razão de um dia a cada três dias trabalhados, segundo a lei das execuções penais, LEI Nº 7.210, DE 11 DE  JULHO DE 1984. O reeducando será remunerado pelas suas atividades laborais.</p>
                        </div>
                        <div class="large-6 columns">
                            <img src="images/site/reeducando.png">
                        </div>
                    </div>
                </div>
            </div>
            <br><br><br>
            <div class="row">
                <div class="large-9 columns large-centered">
                    <center><span><b>Reeducandos dos regimes fechado e semi-aberto</b><br>
                        Secretaria Executiva de Ressocialização - SERES | Gerência de Educação e Qualificação Profissionalizante- GEQP.  Endereço: rua do Hospício, 157, Parque Treze de Maio, Recife-PE.<br>
                        Fone: 3184-2170/99488-s2128.   Email: geqp@seres.pe.gov.br
                      </span></center>
                </div>
            </div>
            <div class="row">
                <div class="large-4 columns large-centered">
                    <br><br><img src="images/site/pe.jpg"><br><br>
                </div>
            </div>
        </div>
    </div>
    @section('scripts')
        @include('site.site-scripts')
    @show
  </body>
</html>