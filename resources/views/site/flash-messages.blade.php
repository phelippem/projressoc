@if(count($errors))
    <div class="form-group">
        <div data-alert class="alert-box alert radius">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
            {{--<a href="#" class="close">&times;</a>--}}
        </div>
    </div>
@endif

@if(session('msg-success'))
    <div class="form-group">
        <div data-alert class="alert-box success radius">
            <ul>
                @foreach(session('msg-success') as $message)
                    <li>{{$message}}</li>
                @endforeach
            </ul>
            {{--<a href="#" class="close">&times;</a>--}}
        </div>
    </div>
@endif

@if(session('msg-infos'))
    <div class="form-group">
        <div class="alert-box warning radius">
            <ul>
                @foreach(session('msg-infos') as $message)
                    <li>{{$message}}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

{{--<div data-alert class="alert-box success radius">
    This is a success alert with a radius.
    <a href="#" class="close">&times;</a>
</div>--}}
