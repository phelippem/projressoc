<!doctype html>
<html class="no-js" lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SIRESPE - Sistema de Reinserção Social do Estado de Pernambuco</title>
    <link rel="stylesheet" href="css/foundation.min.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="css/jquery.fancybox.min.css">

  </head>
  <body>
    <div class="fullwight row">
      <img src="img/topo.jpg" class="first_img">
        <div class="large-8 columns seres large-centered">
          <center><a href="#"><img src="img/seres.png"></a><br><br>
          <span>Sistema de Reinserção Social do<br>
Estado de Pernambuco - SIRESPE</span><br><br>
          <a data-fancybox data-src="#hidden-content-2" href="javascript:;" class="button">SOBRE O PROJETO</a>
          </center>
        </div>
    </div>
    <div style="display: none;" id="hidden-content-2">
      <h4><center>Sistema de Reinserção Social do Estado de Pernambuco SIRESPE</center></h4>
<p>O Programa de Reinserção Social do Estado de Pernambuco tem como objetivo unir esforços Governamentais e  Empresariais com o fim de relocar no mercado de trabalho cidadãos apenados. O SIRESPE contará com um banco de atribuições profissionais e  avaliações periódicas que facilitarão a aproximação entre empresas e apenados. Nos regimes aberto e semi-aberto o apenado se dirigirá ao local de trabalho externo, no caso do regime fechado o Empresário fará uma bancada de trabalho no estabelecimento prisional. O Empresário que desejar contratar, após consulta ao sistema, bastará contactar à Secretaria de Ressocialização, no caso de apenados nos regimes fechado e semi-fechado e  Patronato no regime aberto.</p>
    </div>
    <div class="row">
      <div class="large-12 columns conteudo">
        <div class="row">
            <div class="large-3 columns large-centered"><br>
                <form class="row">
                  <div class="large-10 columns search">
                      <input type="text" placeholder="Buscar talento"/>
                  </div>
                  <div class="large-2 columns lupa">
                      <a href="#"><img src="img/search.jpg"></a>
                  </div>
                </form>
            </div>
        </div>
        <div class="row">
          <div class="large-10 columns large-centered filtro">
            <form class="row">
              <div class="large-3 columns">
                <label>Faixa etária
                   <select>
                      <option value="18-25">18-25 Anos</option>
                      <option value="25-30">25-30 Anos</option>
                      <option value="30-35">30-35 Anos</option>
                      <option value="35-40">35-40 Anos</option>
                   </select> 
                </label>
              </div>
              <div class="large-3 columns">
                <label>Gênero
                   <select>
                      <option value="Masculino">Masculino</option>
                      <option value="Feminino">Feminino</option>
                   </select> 
                </label>
              </div>
              <div class="large-3 columns">
                <label>Profissão
                   <select>
                      <option value="Serviços gerais">Serviços gerais</option>
                      <option value="Secretaria">Secretaria</option>
                      <option value="Vendedora">Vendedora</option>
                      <option value="Cozinheira">Cozinheira</option>
                      <option value="Embaladora">Embaladora</option>
                      <option value="Costureira">Costureira</option>
                      <option value="Auxiliar de cozinha">Auxiliar de cozinha</option>
                      <option value="Técnica de enfermagem">Técnica de enfermagem/option>
                      <option value="Enfermeira">Enfermeira</option>
                      <option value="Camareira">Camareira</option>
                      <option value="Cabeleireira">Cabeleireira</option>
                      <option value="Manicure">Manicure</option>
                      <option value="Caixa de supermercado">Caixa de supermercado</option>
                      <option value="Costureiro de máquina reta e overloque">Costureiro de máquina reta e overloque</option>
                      <option value="Modelista de roupas">Modelista de roupas</option>
                      <option value="Padeiro">Padeiro</option>
                      <option value="Mecânico de máquinas de costura">Mecânico de máquinas de costura</option>
                      <option value="Eletricista de automóveis">Eletricista de automóveis</option>
                      <option value="Eletricista instalador predial de baixa tensão">Eletricista instalador predial de baixa tensão</option>
                      <option value="Pedreiro de alvenaria">Pedreiro de alvenaria</option>
                      <option value="Operador de computador">Operador de computador</option>
                      <option value="Encanador instalador predial">Encanador instalador predial</option>
</option>
                   </select> 
                </label>
              </div>
              <div class="large-3 columns">
                <label>Regime
                   <select>
                      <option value="Fechado">Fechado</option>
                      <option value="Semi-aberto">Semi-aberto</option>
                   </select> 
                </label>
              </div>
              <div class="large-2 columns large-centered">
                <a href="#" class="button">Filtrar</a>
              </div>
            </form>
          </div>
        </div>
        <div class="row beneficios">
          <div class="row">
                <div class="large-10 columns large-centered">
                  <div class="large-6 columns">
                    <h2>Benefícios à Sociedade</h2>
                    <p>O ganho econômico e social de mais um cidadão integrante ao mercado de trabalho é positivamente imensurável. Além de  reduzir a probabilidade do apenado  reincidir criminalmente. O trabalho é dever social, dignifica o homem e tem finalidade educativa e produtiva.</p>
                </div>
                <div class="large-6 columns">
                  <img src="img/apenado.jpg">
                </div>
            </div>
          </div><br><br>
          <div class="row">
              <div class="large-10 columns large-centered">
              <div class="large-6 columns">
                <img src="img/apenado.jpg">
              </div>
              <div class="large-6 columns">
                <h2>Benefícios ao empregador</h2>
                <p>O empregador, no caso do apenado encarcerado, não arcará com obrigações sociais pois não está adstrito a CLT nos termos do art 28 lei das execuções penais.  Terá ainda como vantagem a possibilidade de remunerar inferior ao salário mínimo desde que não seja inferior a ¾ do salário mínimo vigente, conforme art. 29 da lei das execuções.</p>
              </div>
            </div>
          </div><br><br>
          <div class="row">
              <div class="large-10 columns large-centered">
              <div class="large-6 columns">
                  <h2>Benefícios ao apenado</h2>
                  <p>O apenado em regime fechado e semi-aberto poderá ter sua pena reduzida mediante o trabalho regular à razão de um dia a cada três dias trabalhados, segunda a lei das execuções , LEI Nº 7.210, DE 11 DE JULHO DE 1984.</p>
                </div>
              <div class="large-6 columns">
                <img src="img/apenado.jpg">
              </div>              
            </div>
          </div>
        </div>
        <div class="row">
          <div class="large-4 columns large-centered">
            <br><br><img src="img/pe.jpg"><br><br>
          </div>
        </div>
      </div>
    </div>
    <script src="js/vendor/jquery.js"></script>
    <script type="text/javascript">
      $(".search input").focus(function() {
          $('.filtro').hide('slow');       
          //return false;
        });      
     $('.search input').blur(function(){
        if( !$(this).val() ) {
              $('.filtro').show('slow');
        }
    });
    </script>
    <script src="js/vendor/what-input.js"></script>
    <script src="js/vendor/foundation.js"></script>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/vendor/jquery.fancybox.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>