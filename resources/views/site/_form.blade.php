<form action="{{route('site.resultado')}}" method="GET" class="large-12 columns">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="row text-center">
        <h2>Buscar Talento</h2>
    </div>
    {{--<div class="row">
        <div class="large-3 columns large-centered"><br>
            <div class="row">
              <div class="large-10 columns search">
                  <input type="text" name="reeducando_nome" placeholder="Buscar talento"/>
              </div>
              <div class="large-2 columns lupa">
                  <a href="#"><img src="images/site/search.jpg"></a>
              </div>
            </div>
        </div>
    </div> --}}
    <div class="row">
      <div class="large-10 columns large-centered filtro">
        <div class="row">
          <div class="large-3 columns">
            <label>Faixa etária
                <select name="reeducando_idade">
                    <option value="">Selecione</option>
                    <option value="18-25">18-25 Anos</option>
                    <option value="26-30">26-30 Anos</option>
                    <option value="31-35">31-35 Anos</option>
                    <option value="36-40">36-40 Anos</option>
               </select>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Gênero
               <select name="reeducando_sexo">
                   <option value="">Selecione</option>
                   <option value="1" {{ (isset($preso) ? $preso->genero == 1 ? 'selected' : ''  : '' )}}>{{ config('helper.genero.1') }}</option>
                   <option value="2" {{ (isset($preso) ? $preso->genero == 2 ? 'selected' : ''  : '' )}}>{{ config('helper.genero.2') }}</option>
               </select>
            </label>
          </div>
          <div class="large-3 columns">
            <label>Profissão
               <select name="reeducando_profissao">
                   <option value="">Selecione</option>
                   @foreach($profissoes as $pr)
                       <option value="{{$pr->id}}" {{ (isset($preso) ? ( $preso->profissao_id == $pr->id ? 'selected' : 'not' ) : '' ) }}>{{ $pr->nome }}</option>
                   @endforeach
               </select>
            </label>
          </div>
          {{--<div class="large-3 columns">
            <label>Regime
               <select name="reeducando_regime">
                   <option value="">Selecione</option>
                   @foreach(config('helper.regimes') as $k=>$regime)
                       <option value="{{$k}}" {{ ( isset($preso) ? ( $preso->regime == $k ? 'selected' : '' ) : '' )}} >{{ $regime  }}</option>
                   @endforeach
               </select>
            </label>
          </div>--}}
          <div class="large-2 columns large-centered">
            <button type="submit" class="button">Filtrar</button>
          </div>
        </div>
      </div>
    </div>
</form>
<div class="row beneficios">
    <div class="large-10 columns large-centered">
        <center><h2>PRORESPE</h2></center>
        <p>O Programa de Reinserção Social do Sistema Prisional do Estado de Pernambuco tem como objetivo unir esforços Governamentais e  Empresariais com o fim de inserir no mercado de trabalho cidadãos provenientes do sistema prisional. O PRORESPE disponibilizará um banco de atribuições profissionais e supervisões periódicas realizadas por uma equipe multidisciplinar que facilitará a aproximação entre empresas, reeducandos e sociedade. Nos regimes aberto e semi-aberto o reeducando se dirigirá ao local de trabalho externo, no caso do regime fechado, haverá um local adequado, com  segurança, para a empresa desenvolver suas atividades laborais dentro do estabelecimento prisional. O Empresário que desejar contratar o reeducando, após consulta ao sistema, bastará contactar à Secretaria Executiva de Ressocialização. </p>
    </div>
</div>
<hr>