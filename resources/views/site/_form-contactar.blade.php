<input type="hidden" name="_token" value="{{ csrf_token() }}">
{{--<input type="hidden" name="p_id" value="{{ $presidiario_id }}">
<input type="hidden" name="pres_id" value="{{ $presidio_id }}">--}}
{{ method_field('POST') }}
<div class="row">
    <div class="large-3 columns">
        <label>Razão Social
            <input type="text" name="contact-razao" placeholder="Razão Social" />
        </label>
    </div>
    <div class="large-3 columns">
        <label>Nome fantasia*
            <input type="text" required name="contact-nfantasia" placeholder="Nome fantasia" />
        </label>
    </div>
    <div class="large-2 columns">
        <label>CNPJ
            <input type="text" name="contact-cnpj" placeholder="CNPJ" />
        </label>
    </div>
    <div class="large-2 columns">
        <label>Segmento*
            {{--<input type="text" required name="contact-segmento" placeholder="Segmento" />--}}
            <select name="contact-segmento" id="">
                <option value="comercio">Comércio</option>
                <option value="industria ">Indústria</option>
                <option value="servicos">Serviços</option>
            </select>
        </label>
    </div>
    <div class="large-2 columns">
        <label>Ano de fundação*
            <input type="text" required name="contact-anofundacao" placeholder="Ano Fundação" />
        </label>
    </div>
</div>
<div class="row">
    <div class="large-6 columns">
        <label>Diretor responsável*
            <input type="text" required name="contact-responsavel" placeholder="" />
        </label>
    </div>
    <div class="large-3 columns">
        <label>Tel*
            <input type="text" required name="contact-tel" placeholder="Nome fantasia" />
        </label>
    </div>
    <div class="large-3 columns">
        <label>E-mail*
            <input type="text" required name="contact-email" placeholder="Nome fantasia" />
        </label>
    </div>
</div>
<div class="row">
    <div class="large-6 columns">
        <label>Endereço*
            <input type="text" required name="contact-endereco" placeholder="Razão Social" />
        </label>
    </div>
    <div class="large-3 columns">
        <label>Número*
            <input type="text" required name="contact-numero" placeholder="Nome fantasia" />
        </label>
    </div>
    <div class="large-3 columns">
        <label>Bairro*
            <input type="text" required name="contact-bairro" placeholder="Nome fantasia" />
        </label>
    </div>
</div>
<div class="row">
    <div class="large-12 columns">
        <label>Observação*
            <textarea name="contact-observacao" placeholder="Utilize esse espaço para descrever o perfil desejado" rows="5" style="resize: none;"></textarea>
        </label>
    </div>
</div>
<div class="row">
    <div class="large-1 columns large-centered">
        <button type="submit" class="button">Enviar</button>
    </div>
</div>
<div class="row">
    <div class="large-9 columns large-centered">
        <p>As mensagens serão enviadas aos servidores do PRORESPE, em breve entraremos em contato </p>
    </div>
</div>