<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en" class="no-js" dir="ltr">

@section('head')
    @include('site.site-head')
@show

<body>
    <div class="fullwight row">
      <img src="images/site/topo.jpg" class="first_img">
        <div class="large columns seres large-centered">
            <center><a href="/"><img src="images/site/seres.png"></a><br><br>
                <span>Programa de Reinserção Social do Sistema Prisional<br>
                    do Estado de Pernambuco</span><br><br>
                <a data-fancybox data-src="#hidden-content" data-fancybox data-width="80%" data-src="#hidden-content" href="javascript:;" class="button">ENTRE EM CONTATO</a>
                <div style="display: none;" id="hidden-content">
                    <h4><center>Contactar Trabalhador</center></h4>
                    <form action="{{route('site.contactar')}}" method="post">
                        @include('site._form-contactar')
                    </form>
                </div>
            </center>
        </div>
    </div>
    <div style="display: none; width: 60%;" id="hidden-content-projeto">
      <h4><center>Sistema de Reinserção Social do Estado de Pernambuco SIRESPE</center></h4>
<p>O Programa de Reinserção Social do Estado de Pernambuco tem como objetivo unir esforços Governamentais e  Empresariais com o fim de relocar no mercado de trabalho cidadãos apenados. O SIRESPE contará com um banco de atribuições profissionais e  avaliações periódicas que facilitarão a aproximação entre empresas e apenados. Nos regimes aberto e semi-aberto o apenado se dirigirá ao local de trabalho externo, no caso do regime fechado o Empresário fará uma bancada de trabalho no estabelecimento prisional. O Empresário que desejar contratar, após consulta ao sistema, bastará contactar à Secretaria de Ressocialização, no caso de apenados nos regimes fechado e semi-fechado e  Patronato no regime aberto.</p>
    </div>
    <div class="row">
      <div class="large-12 columns conteudo">
          {{--@include('site._form')--}}

          @if(count($results))
              <ul class="row resultados">
                @foreach($results as $res)
                  <li>
                    <div class="row">
                      <div class="large-10 large-centered content_preso">
                        <div class="row">
                          <div class="large-3 columns">
                            <h3>Reeducando {{$res->id}}</h3>
                            <p><span>Idade:</span>{{$res->getAge()}}<br>
                              <span>Presídio:</span> {{$res->presidio->nome}}<br>
                              <span>Regime:</span> {{config('helper.regimes.'.$res->regime)}}<br>
                              <span>Profissão:</span> {{$res->profissao->nome}}<br>
                            </p>
                          </div>
                          <div class="large-9 columns">
                            <p><span>Perfil profissional:</span> {{$res->perfil_profissional}}</p>
                            {{--<p><span>Observação:</span> Existem muitas variações disponíveis de passagens de Lorem Ipsum, mas a maioria sofreu algum tipo de alteração, seja por inserção de passagens com humor, ou palavras aleatórias que não parecem nem um pouco convincentes--}}
                          </div>
                          {{--<div class="large-3 columns large-centered">
                            <a class="cta_preso" data-fancybox data-width="80%" data-src="#hidden-content-{{$res->id}}" href="javascript:;">Fale Conosco</a>
                          </div>
                          <div style="display: none;" id="hidden-content-{{$res->id}}">
                            <h4><center>Contactar Trabalhador</center></h4>
                            <form action="{{route('site.contactar')}}" method="post">
                              @include('site._form-contactar',
                              ['presidio_id' => $res->presidio_id, 'presidiario_id' => $res->id])
                            </form>
                          </div>--}}
                        </div>
                      </div>
                    </div>
                  </li>
                @endforeach
              </ul>
            <div class="row">
                {!! $results->appends(['reeducando_idade'=>$pres_idade, 'reeducando_sexo'=>$pres_sexo, 'reeducando_profissao'=>$pres_profissao])->links() !!}
            </div>
          @else
              <div class="row">
                  <br><br><br>
                <div class="large-10 large-centered content_preso">
                    <div data-alert class="alert-box warning round">
                        <h5>Nenhum resultado encontrado com os critérios informados</h5>
                    </div>
                </div>
              </div>
          @endif
          <br><br><br>
          <div class="row">
              <div class="large-9 columns large-centered">
                  <center><span><b>Reeducandos dos regimes fechado e semi-aberto</b><br>
                        Secretaria Executiva de Ressocialização - SERES | Gerência de Educação e Qualificação Profissionalizante- GEQP.  Endereço: rua do Hospício, 157, Parque Treze de Maio, Recife-PE.<br>
                        Fone: 3184-2170/99488-s2128.   Email: geqp@seres.pe.gov.br
                      </span></center>
              </div>
          </div>
          <div class="row">
              <div class="large-4 columns large-centered">
                  <br><br><img src="images/site/pe.jpg"><br><br>
              </div>
          </div>
      </div>
    </div>
    @section('scripts')
        @include('site.site-scripts')
    @show
</body>
</html>