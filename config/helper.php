<?php

return [

    'roles' => [
		'1' => 'admin',
		'2' => 'diretor',
	],

    'regimes' => [
		1 => 'fechado',
		2 => 'semi-aberto',
		3 => 'aberto'
	],

	'genero' => [
		'1' => 'masculino',
		'2' => 'feminino'
	],

	'status' => [
		'0' => 'inativo',
		'1' => 'ativo'
	],

	'conceito_preso' => [
		'1' => 'insatisfatório',
		'2' => 'regular',
		'3' => 'bom',
		'4' => 'ótimo'
	],


];
